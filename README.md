# premeanApp

This webapp was used to collect data for a user-study run at the University of Glasgow. A research paper was published using this data; it is available at www.horatiubota.com.

The webapp was built using Mongo, Angular, Express and Node.