'use strict';

angular.module('premeanApp')
  .factory('Session', function ($resource) {
    return $resource('/api/session/');
  });
