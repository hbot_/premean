'use strict';

angular.module('premeanApp')
  .factory('Slot', function ($resource) {
    return $resource('/api/slots/update', 
      {}, { update: {
        method: 'PUT',
        params: {}
      }
    }
  );
});