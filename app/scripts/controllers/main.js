'use strict';

angular.module('premeanApp')
.controller('MainCtrl', function ($scope, $http, $rootScope, $location, $resource, User, Auth, Slot) {

  $scope.selectedTopics = 0;

  $scope.signupTask = function () {
    $scope.task = 'signup';    
  }

  $scope.selectTask = function () {
    $scope.task = 'selectQueries';    
  }

  $scope.thanksTask = function () {
    $scope.task = 'thanks';    
  }

  $scope.user = {};
  $scope.errors = {};

  // Find all available slots;
  $http.get('/api/slots').success(function (slots) {
    $scope.slots = slots;
    $scope.selectedSlot = $scope.slots[0];
  });

  // Get all possible queries
  $http.get('/api/queries').success(function (queries) {
    $scope.queries = queries;
  });

  //$scope.selectTask();

  $scope.toggleTopic = function (query) {
    if (query.selected) {
      $scope.selectedTopics--;
    } else {
      $scope.selectedTopics++;
    }
    
    query.selected = !query.selected;

  }

  $scope.familiarityOver = function(query, value) {
    query.familiarityOver = value;
  };

  $scope.familiarityLeft = function(query) {
    query.familiarityOver = undefined;
  }

  $scope.complexityOver = function(query, value) {
    query.complexityOver = value;
  }

  $scope.complexityLeft = function(query) {
    query.complexityOver = undefined;
  }

  $scope.getCriterionText = function (value, criterion) {
    if (value >= 1 && value <= 2) {
      return "Not at all " + criterion;
    } else if (value > 2 && value <= 4) {
      return "Slightly " + criterion;
    } else if (value > 4 && value <= 6) {
      return "Somewhat " + criterion;
    } else if (value > 6 && value <= 8) {
      return "Moderately " + criterion;
    } else if (value > 8) {
      return "Very " + criterion;
    } else {
      return undefined;
    }
  }

  $scope.signup = function(form) {
    $scope.submitted = true;

    if(form.$valid) {
     Auth.createUser({
      name: $scope.user.name,
      email: $scope.user.email
    }).then(function(user) {

      $scope.selectedSlot.userName = $scope.user.name;
      $scope.selectedSlot.userEmail = $scope.user.email;
      
      Slot.update($scope.selectedSlot);
      
      // Go to setup
      $scope.selectTask();

    }, function (err) {
      console.log(err);

    }).catch( function(err) {
          err = err.data;
          $scope.errors = {};

          // Update validity of form fields that match the mongoose errors
          angular.forEach(err.errors, function(error, field) {
            form[field].$setValidity('mongoose', false);
            $scope.errors[field] = error.message;
          });
        });
      }
    };

  $scope.submitPreferences = function () {
    var topics = [];
    for (var i = 0; i < $scope.queries.length; i++) {
      if ($scope.queries[i].selected){
        // In case the user did not assess
        // topic knowledge, make it 0
        if (!$scope.queries[i].familiarity){
          $scope.queries[i].familiarity = 0;
        }

        if (!$scope.queries[i].complexity){
          $scope.queries[i].complexity = 0;
        }

        topics.push($scope.queries[i]);
      }
    }

    console.log(topics);
    console.log("SENDING UPDATE");

    var User = $resource('/api/users/update', {},
      { 
        update: {
         method: 'POST',
         params: {}
        }
     });

    $rootScope.currentUser.topics = topics;
    User.update($rootScope.currentUser, function(data) {
      $scope.thanksTask();
    });


  }

  $scope.userSubmitPreferences = function() {

    if ($scope.selectedTopics < 10) {
      var confirmation = confirm("It would really help us if you chose 10 topics. Are you sure you want to submit without choosing 10 topics?");
      if (confirmation) {
        $scope.submitPreferences();
      } 
    } else {
      $scope.submitPreferences();
    }
  };
});