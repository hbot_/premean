'use strict';

var mongoose = require('mongoose'),
    Slot = mongoose.model('Slot');

/**
 * Create slot
 */
// exports.create = function (req, res) {
//   var newSlot = new Slot(req.body);

//   newSlot.save(function(err) {
//     if (err) return res.json(400, err); 
//     return res.json({"message": "Slot created");
//   });
// };


/**
* Get all available slots
*/

exports.getSlots = function (req, res) {  
  return Slot.find({userName: null}, function (err, slots) {
	  	if (!err) {
	  		return res.json(slots);
	  	} else {
	  		return res.send(err);
	  	}
	});
};

exports.updateSlot = function(req, res) {

	Slot.findOneAndUpdate(
		// Query
		{_id: req.body._id, userName: null}, 
		
		// Fields to update
		{
			"userName": req.body.userName,
			"userEmail": req.body.userEmail
		},

		// Callback 
		function (err, slot) {
			if (err) { 
				res.send(400, err); 
			} else if (slot === undefined) {
				res.send(400, "Slot taken by someone else"); 
			} else {
				res.send(200, {"slot":"updated"});
			}
	});
};

/**
 *  Get slot
 */
exports.findByDateString = function (req, res) {
  var slotDateString = req.params.dateString;

  Slot.find({dateString: slotDateString}, function (err, slot) {
    if (err) return res.send(400, err);
    res.send(slot);
  });
};