'use strict';

var mongoose = require('mongoose'),
    Query = mongoose.model('Query');

/**
* Get all available queries
*/

exports.getQueries = function (req, res) {  
  return Query.find(function (err, queries) {
	  	if (!err) {
	  		
	  		queries = shuffle(queries);
	  		queries = queries.slice(0, queries.length / 2);

	  		console.log(queries.length);
	  		
	  		return res.json(queries);
	  	} else {
	  		return res.send(err);
	  	}
	});
};

//Fisher-Yates (aka Knuth) Shuffle
function shuffle(array) {
  var currentIndex = array.length
    , temporaryValue
    , randomIndex
    ;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}
