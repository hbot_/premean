'use strict';

var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    nodemailer = require('nodemailer'),
    passport = require('passport');

/*

Creating email infrastructure

*/

var smtpTransport = nodemailer.createTransport("SMTP",{
    service: "Gmail",
    auth: {
        user: "compositeretrieval@gmail.com",
        pass: "compositeretrieval21"
    }
});

// setup e-mail data with unicode symbols
var mailOptions = {
    from: "User study <compositeretrieval@gmail.com>", // sender address
    to: "h.bota.1@research.gla.ac.uk", // list of receivers
    subject: "New user", // Subject line
    text: "Sers\n" // plaintext body
}

/*

Done with email infrastructure

*/

/**
 * Create user
 */
 exports.create = function (req, res, next) {
  console.log("A NEW USER IS CREATED");
  var newUser = new User(req.body);
  newUser.save(function(err) {
    if (err)  {
      console.log(err);
      return res.json(400, err);
    } else {
      mailOptions.text += "\nS-o inscris cineva pentru studiu:"
      mailOptions.text += '\nNume:' + newUser.name + '\nEmail: ' + newUser.email;
      smtpTransport.sendMail(mailOptions, function(error, response){
        if(error){
          console.log(error);
        }else{
          console.log("Message sent: " + response.message);
        }
      });
      return res.json(200, newUser);
    }
  });
};

exports.update = function(req, res) {

  User.findOneAndUpdate(
    // Query
    {_id: req.body._id}, 
    
    // Fields to update
    {
      topics: req.body.topics,
    },

    // Callback 
    function (err, slot) {
      if (err) { 
        res.send(400, err); 
      } else {
        res.send(200, {"user":"updated"});
      }
  });
};

// /**
//  *  Get profile of specified user
//  */
// exports.show = function (req, res, next) {
//   var userId = req.params.id;

//   User.findById(userId, function (err, user) {
//     if (err) return next(err);
//     if (!user) return res.send(404);

//     res.send({ profile: user.profile });
//   });
// };

// /**
//  * Change password
//  */
// exports.changePassword = function(req, res, next) {
//   var userId = req.user._id;
//   var oldPass = String(req.body.oldPassword);
//   var newPass = String(req.body.newPassword);

//   User.findById(userId, function (err, user) {
//     if(user.authenticate(oldPass)) {
//       user.password = newPass;
//       user.save(function(err) {
//         if (err) return res.send(400);

//         res.send(200);
//       });
//     } else {
//       res.send(403);
//     }
//   });
// };

/**
 * Get current user
 */
exports.me = function(req, res) {
  res.json(req.user || null);
};
