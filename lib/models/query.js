'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    
/**
 * Thing Schema
 */
var QuerySchema = new Schema({
	//this is a UTC date string
	id: String,
	text: String,
	orientation: String
});

mongoose.model('Query', QuerySchema);