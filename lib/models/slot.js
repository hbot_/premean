'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    
/**
 * Thing Schema
 */
var SlotSchema = new Schema({
	//this is a UTC date string
	dateString: String,
	userName: String,
	userEmail: String
});


// SlotSchema
//   .validate(function(value, respond) {
//     var self = this;
//     this.constructor.findOne({email: value}, function(err, user) {
//       if(err) throw err;
//       if(user) {
//         if(self.id === user.id) return respond(true);
//         return respond(false);
//       }
//       respond(true);
//     });
// }, 'Some already signed up using this email address. Please contact me if it wasn\'t you.');

mongoose.model('Slot', SlotSchema);